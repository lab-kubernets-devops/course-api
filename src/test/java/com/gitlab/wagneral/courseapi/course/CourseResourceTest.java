package com.gitlab.wagneral.courseapi.course;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CourseResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @DisplayName("Deve retornar os cursos com sucesso.")
    public void shouldReturnSuccessFullyReturnTheCourses() throws Exception {
        ResultActions returns = mockMvc.perform(get("/api/courses")
                .contentType(MediaType.APPLICATION_JSON));

        returns.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    @DisplayName("Deve retornar apenas um curso.")
    public void shouldReturnOneCourse() throws Exception {
        var response = mockMvc.perform(get("/api/courses?size=1")
                .contentType(MediaType.APPLICATION_JSON));

        response.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        String content = response.andReturn().getResponse().getContentAsString();
        var courses = serializeCourses(content);

        assertTrue (courses.size() == 1);

    }

    private List<Course> serializeCourses(String body) throws JsonProcessingException {
        return objectMapper.readValue(body, objectMapper.getTypeFactory().constructCollectionType(List.class, Course.class));
    }
}
