package com.gitlab.wagneral.courseapi.course;

public enum CourseType {

    BUSINESS, TECHNOLOGY, LANGUAGE
}
