package com.gitlab.wagneral.courseapi.course.vo;

import lombok.Getter;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Getter
public class Price {

    public static int SCALE = 2;
    private BigDecimal value;

    public Price(final double value) {
        this.value = new BigDecimal(value).setScale(SCALE, RoundingMode.HALF_UP);
    }
    Price(){};
}
