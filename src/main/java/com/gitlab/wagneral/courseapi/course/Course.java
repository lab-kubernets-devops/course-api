package com.gitlab.wagneral.courseapi.course;

import com.gitlab.wagneral.courseapi.course.vo.Price;
import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Builder
@Getter
public class Course {

    private UUID id;
    private CourseType type;
    private String title;
    private String description;
    private Price price;
}
