package com.gitlab.wagneral.courseapi.course;

import com.gitlab.wagneral.courseapi.course.vo.Price;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/courses")
public class CourseResource {

    @Value("${app.max-size-page}")
    private int maxSizePage;

    @GetMapping
    public ResponseEntity<?> getAllCourses(
            @RequestParam(defaultValue = "100", name = "size", required = false) final int size
    ) {
        return ResponseEntity.ok(listCourses(size));
    }

    private List<Course> listCourses(int size) {

        return List.of(
                Course.builder()
                        .id(UUID.randomUUID())
                        .title("NoSQL com mongoDB")
                        .type(CourseType.TECHNOLOGY)
                        .description("Curso de NoSQL com mongoDB.")
                        .price(new Price(899.80))
                        .build(),
                Course.builder()
                        .id(UUID.randomUUID())
                        .title("Investimentos")
                        .type(CourseType.BUSINESS)
                        .description("Cursos voltado para pessoas que querem aumentar seus redimentos.")
                        .price(new Price(2090.40))
                        .build(),
                Course.builder()
                        .id(UUID.randomUUID())
                        .title("Inglês Americano")
                        .type(CourseType.LANGUAGE)
                        .description("Aprenda inglês em 12 meses.")
                        .price(new Price(899.80))
                        .build()
        ).stream().limit(size < 0 || size > maxSizePage ? maxSizePage : size)
                .collect(Collectors.toList());
    }
}
